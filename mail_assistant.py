"""
    This module offers a commandline-interface to either
        - download mail attachments from an IMAP server
        - or send previously downloaded attachments back to
          the sender (SMTP server)

    All the communication with IMAP and SMTP servers is done via SSL.
    The user is prompted for the passwords not until they are used to
    establish a connection. The passwords are prompted with help of the
    python module getpass. The passwords are not stored in this script nor
    one of the logfiles.

    Copyright (C) 2015  Sven Lieber

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import sys
import argparse
import time
import logging
import json
import mail_utils as mu
from tabulate import tabulate

__author__ = "Sven Lieber"
__copyright__ = "Copyright (C) 2015 Sven Lieber"
__license__ = "GPL 3.0"
__version__ = "1.0.0"
__email__ = "Sven.Lieber@gmail.com"

#----------------------------------------------------------
SCRIPT_TIME = time.strftime("%Y-%m-%d-%H-%M-%S")
MESSAGE_FILE_NAME = "email-content.txt"
DEFAULT_MAIL_CONTENT = "This mail was sended automatically"

# If mails could not be send, they are listet in this file
OUTSTANDING_FILE_NAME = "outstanding_" + SCRIPT_TIME + ".log"


#----------------------------------------------------------
def main():

    args = process_cmd_args()
    logfile_name = "assistant_log_" + SCRIPT_TIME + ".log"
    init_logger(logfile_name, args.is_debug)

    if(args.imap_server):
        # Mode to download mail attachments
        fetch_mails(args.username,
                    args.imap_server,
                    args.download_path,
                    args.suffix)
    elif(args.smtp_server):
        # Mode to send mail attachments back to sender
        send_mails(args.username,
                   args.received_file,
                   args.content_file,
                   args.subject,
                   args.sender,
                   args.smtp_server)


# -------------------------------------------------------------
def fetch_mails(username, imap_server, download_path, suffix):
    """ This function tries to connect to the given imap_server
        with the given username. The communication is done via SSL,
        the user is prompted for a password (python getpass module is
        used).

        When the connection is established, the user can select one
        of his mailboxes and can search for mails within this mailbox.

        Search-Criteria are a Date-Range and/or strings which should
        appear in the mail subjects.
    """

    con = mu.IMAPConnector(username, imap_server)
    mailboxes = con.list_mailboxes()

    m_index = ask_user_for_mailbox(mailboxes)
    con.select_mailbox(mailboxes[m_index])

    dates = ask_for_date_range()

    msg_ids = con.get_mail_ids(dates[0], dates[1])
    msg_ids = ask_user_for_mails(msg_ids, con)

    con.download_attachments(msg_ids, download_path, suffix)


# -------------------------------------------------------------
def ask_user_for_mails(msg_ids, con):
    """ Show the user meta data of the given msg_ids and
        offer him the possibility to deselect message ids.
        The result list then contains only the message ids,
        the user doesn't deselect.
    """

    if(not msg_ids):
        sys.exit("No mails found!")

    result_ids = list()
    int_ids = sorted([int(x) for x in msg_ids])
    mail_tuples = con.get_overview(msg_ids)

    headers = ["id", "Attachment", "Subject", "Sender"]
    print(tabulate(mail_tuples, headers=headers, tablefmt="orgtbl"))

    info_text = "EXCLUDE mails from downloading (space separated list): "
    excludes = ask_for_multiple_numbers(info_text,
                                        int_ids[0],
                                        int_ids[-1])

    for msg_id in msg_ids:
        if(int(msg_id) not in excludes):
            result_ids.append(msg_id)
    return(result_ids)


# -------------------------------------------------------------
def ask_for_date_range():
    """ Asks the user for two dates (the second is optional).
    """
    start_date = None
    end_date = None
    while(True):
        start_date_string = input("Give a start date, e.g. 26-11-2015: ")
        try:
            start_date = time.strptime(start_date_string, "%d-%m-%Y")

            while(True):
                end_date_string = input("Give an optional end date: ")
                try:
                    end_date = time.strptime(end_date_string, "%d-%m-%Y")
                except Exception as e:
                    if(end_date_string):
                        print("Try again: " + str(e))
                    else:
                        # skip if empty (end_date is optional)
                        break
                break
            break
        except Exception as e:
            print("Try again: " + str(e))
    return([start_date, end_date])


# -------------------------------------------------------------
def send_mails(username, addressbook_file, message_file, subject, sender,
               smtp_server):
    """ This function tries to connect to the given smtp_server with the given
        username. The communication is done via SSL, the user is prompted
        for a password (python getpass module is used).

        When the connection is established, the user can manually deselect
        entries of the given addressbook json file. When done, an email
        with the content of the given message_file is send to all addresses
        in the given addressbook file. Also all files listed in the addressbook
        file, belonging to a specific address, are attached to the mail.

        Pressing enter in the deselection phase just skips the deselection.
    """
    logger = logging.getLogger(__name__)
    logger.debug("Try to open '" + addressbook_file + "'")
    try:
        with open(addressbook_file) as data_file:
            data = json.load(data_file)
    except OSError:
        sys.exit("Could not open '" + addressbook_file + "'")
    except ValueError:
        sys.exit("Could not parse '" + addressbook_file + "'")

    outstanding = list()
    content = ""
    try:
        with open(message_file) as msg_file:
            content = msg_file.read()
    except OSError:
        content = "This mail was sended automatically from " + sender

    # Getting receiver data, result is e.g.
    # [
    #  [["\"John Doe\" <jon.doe@example.com>"],
    #   ["file1.pdf", "path/to/file2.jpg"]
    #  ],
    #  [["<test@example.com>"],
    #   []
    #  ]
    # ]
    receiver = ask_user_for_sending(data)

    con = mu.SMTPConnector(username, sender, smtp_server)

    for r in receiver:
        try:
            logger.info("Sending mail to: " + r[0])
            if(r[1]):
                logger.info("\tSend files: " + str(r[1]))
                con.send_mail([r[0]], subject, content, r[1])
            else:
                logger.info("No files to send")

        except Exception as e:
            logger.error("Could not send mail to: " + str(r[0]))
            logger.error(str(e))
            outstanding.append(r)

    if(outstanding):
        logger.info("Some mails couln't be send, look at: "
                    + OUTSTANDING_FILE_NAME)
        with open(OUTSTANDING_FILE_NAME, "w") as text_file:
            text_file.write(json.dumps(outstanding))


# -------------------------------------------------------------
def ask_user_for_mailbox(mailboxes):
    """ Display a list with "[id] name" where the user
        can select one value.
    """

    for i, mbox in enumerate(mailboxes):
        print("[" + str(i) + "]\t" + mbox)

    m_index = ask_for_number("Select Mailbox: ",
                             0,
                             (len(mailboxes) - 1))
    return(m_index)


# -------------------------------------------------------------
def ask_for_number(text, number_min, number_max):
    """ Asked the user in a loop to give a number
        between number_min and number_max.
    """
    index = 0
    while(True):
        selected = input(text)
        try:
            index = int(selected)
            if(index >= number_min and index <= number_max):
                break
            else:
                print("number is out of array bounds")
        except ValueError:
            print("Invalid number")
    return(index)


# -------------------------------------------------------------
def ask_for_multiple_numbers(text, number_min, number_max):
    """ Asked the user in a loop to give numbers delimited by
        whitespace. Each number must be between number_min and number_max.
    """
    indeces = list()
    fail = False
    while(True):
        del indeces[:]
        fail = False
        selected = input(text)
        try:
            numbers = selected.split()
            for n in numbers:
                n = int(n)
                if(n < number_min or n > number_max):
                    fail = True
                else:
                    indeces.append(int(n))
            if(fail):
                print("One number is out of array bounds")
            else:
                break
        except:
            print("Invalid number")
    return(indeces)


# -------------------------------------------------------------
def ask_user_for_sending(receiver_dict):
    """ Given a dictionary where the key is a mail address and the
        value is a list of filenames, the user is asked to deselect
        one or more entries. Also only enter can be pressed to skip.

        The result of the function is a list of lists, where each
        sub-list contains the email address at first element and
        another sub-list as second element where the filenames are
        stored.
    """
    receiver_list = list()
    send_to = list()
    for key in receiver_dict:
        receiver_list.append([key, receiver_dict[key]])

    for i, receiver in enumerate(receiver_list):
        print("[" + str(i) + "]\t" + receiver[0])

    info_string = "EXCLUDE someone from sending (space separated list):  "
    excludes = ask_for_multiple_numbers(info_string,
                                        0,
                                        (len(receiver_list) - 1))

    for i, receiver in enumerate(receiver_list):
        if(i not in excludes):
            send_to.append(receiver)
    return(send_to)


# -------------------------------------------------------------
def init_logger(filename, is_debug):
    """ Initialize a logger. This function should be called at
        the beginning of the script.
    """
    if(is_debug):
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    logger = logging.getLogger()
    logger.setLevel(log_level)

    log_file_handler = logging.FileHandler(filename)
    log_file_handler.setLevel(log_level)

    log_screen_handler = logging.StreamHandler()
    log_screen_handler.setLevel(logging.INFO)

    mail_format = "[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s"
    formatter = logging.Formatter(mail_format)
    log_file_handler.setFormatter(formatter)

    logger.addHandler(log_file_handler)
    logger.addHandler(log_screen_handler)


# -------------------------------------------------------------
def process_cmd_args():
    """
    Using a standard library to fetch commandline arguments.
    """
    parser = argparse.ArgumentParser(
        description="Retreive and send mails with attachment automatically")

    parser.add_argument("-i", "--imap", action="store",
                        dest="imap_server",
                        help="Retrieve mails from this server.")

    parser.add_argument("-s", "--smtp", action="store",
                        dest="smtp_server",
                        help="Send mails from this server.")

    parser.add_argument("-r", "--receivers", action="store",
                        dest="received_file",
                        help="Attachment files listed in this file are send"
                        + "to the address also listed in this file.")

    parser.add_argument("-u", "--username", action="store",
                        dest="username", required=True,
                        help="The username of the mail account.")

    parser.add_argument("-d", "--debug", action="store_true",
                        dest="is_debug",
                        help="This flag activates the debug mode.")

    parser.add_argument("-c", "--content", action="store",
                        dest="content_file",
                        help="The content of this file will be used as"
                             + " content of the email.")

    parser.add_argument("--download", action="store",
                        dest="download_path",
                        help="The folder in which the downloaded mail"
                        + " attachments are stored.")

    parser.add_argument("--sender", action="store",
                        dest="sender",
                        help="Your Email address (will be used as sender"
                        + " address).")

    parser.add_argument("--suffix", action="store",
                        dest="suffix",
                        default="_copy",
                        help="A copy of each downloaded file is made, which"
                        + " then contains this suffix in the name.")

    parser.add_argument("--subject", action="store",
                        dest="subject",
                        help="The subject of the mails which will be sent.")

    args = parser.parse_args()
    check_cmd_args(args)

    return(args)


# -------------------------------------------------------------
def check_cmd_args(args):
    """
    Some arguments require additional arguments, this function
    checks if they are given.
    """
    if(args.imap_server and args.smtp_server):
        sys.exit("Only one of the arguments 'imap' or 'smtp' is allowed!")

    if(args.imap_server):
        if(not args.download_path):
            sys.exit("Download path parameter needed")

    if(args.smtp_server):
        if(not args.subject):
            sys.exit("The parameter --subject is needed!")
        if(not args.sender):
            sys.exit("The parameter --sender is needed!")
        if(not args.content_file):
            sys.exit("The parameter --content is needed!")

        if(not args.received_file):
            sys.exit("The parameter --receivers is needed!")
        else:
            if(not os.path.isfile(args.received_file)):
                sys.exit("Given File '"
                         + args.received_file + "' doesn't exist")

if __name__ == "__main__":
    main()
