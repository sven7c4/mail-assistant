"""
    This module contains a thin interface to IMAP and SMTP servers.

    Copyright (C) 2015  Sven Lieber

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"
"""

import os
import imaplib
import smtplib
import json
import email
import email.header
import time
import shutil
import logging
import getpass

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

__author__ = "Sven Lieber"
__copyright__ = "Copyright (C) 2015 Sven Lieber"
__license__ = "GPL 3.0"
__version__ = "1.0.0"
__email__ = "Sven.Lieber@gmail.com"

#----------------------------------------------------------
MAIL_RFC = '(RFC822)'
SENDER_ATTACHMENT_FILE_NAME = "received_content.json"


#----------------------------------------------------------
class IMAPConnector(object):
    """ Thin interface to an IMAP mail server.
    """

    #----------------------------------------------------------
    def __init__(self, username, hostname):
        """ The constructor will promt the user for a password
            and tries to establish a connection to the IMAP server
            on the given hostname.
        """
        logger = logging.getLogger(__name__)
        message_prompt = "Try to connect to '" + hostname + "'"\
                         " as user '" + username + "'\n"\
                         "Please enter the password (It will be send"\
                         " via SSL)"

        logger.info(message_prompt)
        self.connection = imaplib.IMAP4_SSL(hostname)
        self.subject_string = ""
        password = getpass.getpass()

        self.connection.login(username, password)

    #----------------------------------------------------------
    def check_response(self, code, data):
        """ Check the value of code, if it is 'NO' an Exception
            with the message in data is raised.
        """
        if(code):
            if(code == 'NO'):
                raise Exception(data)

    #----------------------------------------------------------
    def select_mailbox(self, mailbox):
        """ Select the given mailbox. Selecting according to
            the used imaplib means, all following actions are
            done on the selected mailbox.
        """
        typ, data = self.connection.select('"' + mailbox + '"')
        self.check_response(typ, data)

    #----------------------------------------------------------
    def list_mailboxes(self):
        """ Returns a list of the mailboxes.
        """
        mailboxes = list()
        typ, data = self.connection.list()
        self.check_response(typ, data)

        # One entry looks like: '(\\HasChildren) "/" "INBOX/folder"'
        for mailbox in data:
            parts = mailbox.decode("utf-8").split('"')
            mailboxes.append(parts[len(parts)-2])
        return(mailboxes)

    #----------------------------------------------------------
    def test_search_query(self, query):
        """ This function can be used to send an IMAP search
            query to the server. The found messages ids are
            returned.
        """
        typ, msg_ids = self.connection.search(None, query)
        self.check_response(typ, msg_ids)
        return(msg_ids)

    #----------------------------------------------------------
    def set_subjects(self, subjects):
        """ Given a list of strings which should be part of the
            mail subjects. The subjects are concatenated using or.
        """
        wrapped_subjects = ['"' + x + '"' for x in subjects]
        self.subject_string = ' OR SUBJECT '.join(wrapped_subjects)

    #----------------------------------------------------------
    def get_mail_ids(self, start_date, end_date):
        """ An IMAP search is performed, using the given time
            strings and optional the formerly set mail subjects.
            E.g. get_mail_ids("20-Nov-2015", "22-Nov-2015").
        """
        logger = logging.getLogger(__name__)

        start_date_str = time.strftime("%d-%b-%Y", start_date)
        search_string = '(SINCE "' + start_date_str + '" '
        if(end_date):
            end_date_str = time.strftime("%d-%b-%Y", end_date)
            search_string += 'BEFORE "' + end_date_str + '" '
        if(self.subject_string):
            search_string += 'SUBJECT ' + self.subject_string + ' '
        search_string += ')'

        logger.info("IMAP Search: " + search_string)
        logger.info("Depending on the connection, this could take a while")
        typ, msg_ids = self.connection.search(None, search_string)
        self.check_response(typ, msg_ids)

        msg_ids = [x.decode("utf-8") for x in msg_ids]
        msg_ids = msg_ids[0].split()
        return(msg_ids)

    #----------------------------------------------------------
    def get_overview(self, msg_ids):
        """ Returns a list of tuples (msg_id, has_attachment, subject, sender)
            E.g [("67", "A", "Mail with attachment", "jon doe"),
                 ("71", "-", "Simple Mail", "Mr. X")]
        """
        overview = list()
        for msg_id in msg_ids:
            typ, msg_data = self.connection.fetch(msg_id, MAIL_RFC)
            self.check_response(typ, msg_data)
            for response_part in msg_data:
                if(isinstance(response_part, tuple)):
                    msg = email.message_from_string(
                        response_part[1].decode("utf-8"))
                    has_attachment = "A" if msg.is_multipart() else "-"
                    subject = self.get_decoded_value(msg.get("Subject"))
                    sender = self.get_decoded_value(msg.get("From"))
                    overview.append((msg_id, has_attachment, subject, sender))
        return(overview)

    #----------------------------------------------------------
    def save_attachment(self, payload, file_name, folder):
        """ Saving the given attachment as file_name in the given folder.
            Payload should be an IMAP message object representing
            an attachment.
        """
        logger = logging.getLogger(__name__)
        logger.info("Save attachment '" + file_name
                    + "' in folder '" + folder + "'")

        full_path = os.path.join(folder, file_name)
        if(not os.path.isfile(full_path)):
            fp = open(full_path, 'wb')
            fp.write(payload.get_payload(decode=True))
            fp.close()
        else:
            logger.info(file_name + " already exists!")
        return(full_path)

    #----------------------------------------------------------
    def copy_file(self, fullpath, suffix):
        """ Create a copy of the file with the added suffix.
            E.g copy_file("/home/user/myFile.txt", "_copy")
            results in two files:
                /home/user/myFile.txt
                /home/user/myFile_copy.txt
        """
        logger = logging.getLogger(__name__)
        filename, file_extension = os.path.splitext(fullpath)

        filename += suffix
        new_file = filename + file_extension
        try:
            shutil.copy(fullpath, new_file)
        except Exception as e:
            logger.error("Could not copy file '"
                         + fullpath + "' to '"
                         + new_file + "' " + str(e))
        return(new_file)

    #----------------------------------------------------------
    def download_attachments(self, msg_ids, folder, suffix="_copy"):
        """ Dowload the attachments of the mails with the given ids
            and store them in folder.
        """

        logger = logging.getLogger(__name__)
        sender_info = dict()
        for msg_id in msg_ids:
            logger.debug("Processing mail with msg_id: " + str(msg_id))
            typ, msg_data = self.connection.fetch(msg_id, MAIL_RFC)
            self.check_response(typ, msg_data)
            for response_part in msg_data:
                if(isinstance(response_part, tuple)):
                    msg = email.message_from_string(
                        response_part[1].decode("utf-8"))
                    if(msg.is_multipart()):
                        logger.debug("Mail has attachments")
                        self.handle_multipart_msg(msg, folder, sender_info,
                                                  suffix)
                    else:
                        logger.debug("Mail has no attachment")

        meta_data = os.path.join(folder, SENDER_ATTACHMENT_FILE_NAME)
        logger.info("Saving meta data in '" + meta_data + "'")
        with open(meta_data, "w") as text_file:
            # Ensure_ascii = False, because it may contain utf-8
            text_file.write(json.dumps(sender_info, ensure_ascii=False))

    #----------------------------------------------------------
    def handle_multipart_msg(self, msg, folder, sender_info, suffix):
        """ Downloads the file attachments of the given msg.
        """

        logger = logging.getLogger(__name__)
        for file in msg.get_payload():
            if(file.get_filename() is None):
                logger.debug("Current payload is not a file")
            else:
                file_name = self.get_decoded_value(file.get_filename())
                logger.debug("Current payload is a file: " + str(file_name))
                file_name = self.get_decoded_value(file_name)

                saved_file = self.save_attachment(file,
                                                  file_name,
                                                  folder)

                saved_file_copy = self.copy_file(saved_file, suffix)
                sender = self.get_decoded_value(msg.get("From"))

                if(sender in sender_info):
                    sender_info[sender].append(saved_file_copy)
                else:
                    sender_info[sender] = [saved_file_copy]

    #----------------------------------------------------------
    def get_decoded_value(self, value):
        """ Looks at the given value and try to decode it.
        """
        logger = logging.getLogger(__name__)
        decoded_values = email.header.decode_header(value)

        all_values = list()
        for decoded_value in decoded_values:
            enc_value, enc = decoded_value

            if(enc):
                logger.debug("value encoded with: " + str(enc))
                enc_value = enc_value.decode(enc)
                logger.debug("after decoding: " + str(enc_value))
            else:
                logger.debug("No encoding in: " + str(enc_value))
                if(not isinstance(enc_value, str)):
                    logger.debug("No string, manual decoding")
                    enc_value = enc_value.decode("utf-8")
                    logger.debug("Encoded to: " + str(enc_value))
            enc_value = enc_value.replace("\r", "")
            enc_value = enc_value.replace("\n", "")
            all_values.append(enc_value)

        logger.debug("After encoding: " + str(all_values))
        return(" ".join(all_values))


#----------------------------------------------------------
class SMTPConnector(object):
    """ Thin interface to an SMTP mail server.
    """

    #----------------------------------------------------------
    def __init__(self, username, send_from, hostname):
        """ The constructor will promt the user for a password
            and tries to establish a connection to the SMTP server
            on the given hostname.
        """
        self.connection = smtplib.SMTP_SSL(host=hostname)
        self.send_from = send_from
        password = getpass.getpass()
        self.connection.login(username, password)

    #----------------------------------------------------------
    def send_mail(self, send_to, subject, text, files=[]):
        """ Taken from the web, this function will send an email.
            E.g. send_mail(["<test@example.com>"],
                           "Test", "Content",
                           ["file1.pdf", "file2.jpg"])
        """
        assert type(send_to) == list
        assert type(files) == list

        msg = MIMEMultipart()
        msg['From'] = self.send_from
        msg['To'] = COMMASPACE.join(send_to)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject
        msg.attach(MIMEText(text))
        for f in files:
            part = MIMEBase('application', "octet-stream")
            part.set_payload(open(f, "rb").read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition',
                            'attachment; filename="%s"' % os.path.basename(f))
            msg.attach(part)

        self.connection.sendmail(self.send_from, send_to, msg.as_string())
