# Overview #
This script emerged from the problem of performing the same actions manually again and again.

This script simply downloads mail attachments automatically and allows you to send the downloaded files back to the sender.

# When do I need this script? #

* If you have to download attachments of more than one mail. You can download everything and then process it further, e.g. print it or whatever.

* If you regularly get mails with attachments and have to annotate the attachments. You can use the script to download everything, then you annoate the downloaded files. Together with downloading the meta data which file belongs to whom is also stored. So you can also automated send everything back using this script again.

```
usage: mail_assistant.py [-h] [-i IMAP_SERVER] [-s SMTP_SERVER]
                         [-r RECEIVED_FILE] -u USERNAME [-d] [-c CONTENT_FILE]
                         [--download DOWNLOAD_PATH] [--sender SENDER]
                         [--suffix SUFFIX] [--subject SUBJECT]

Retreive and send mails with attachment automatically

optional arguments:
  -h, --help            show this help message and exit
  -i IMAP_SERVER, --imap IMAP_SERVER
                        Retrieve mails from this server.
  -s SMTP_SERVER, --smtp SMTP_SERVER
                        Send mails from this server.
  -r RECEIVED_FILE, --receivers RECEIVED_FILE
                        Attachment files listed in this file are sendto the
                        address also listed in this file.
  -u USERNAME, --username USERNAME
                        The username of the mail account.
  -d, --debug           This flag activates the debug mode.
  -c CONTENT_FILE, --content CONTENT_FILE
                        The content of this file will be used as content of
                        the email.
  --download DOWNLOAD_PATH
                        The folder in which the downloaded mail attachments
                        are stored.
  --sender SENDER       Your Email address (will be used as sender address).
  --suffix SUFFIX       A copy of each downloaded file is made, which then
                        contains this suffix in the name.
  --subject SUBJECT     The subject of the mails which will be sent.

```

# Download mail attachments #

To Download mail attachments call:
```python
mail_assistant.py 
  -i imap.example.com 
  -u jonDoe
  --suffix _copy
  --download /home/user/Desktop
```

Then you are prompted for the Password of your mail account. The password is fetched via the python library `getpass` and is transmitted via `imaplib.IMAP4_SSL`. After this you get a list of your mailboxes (folders in your mail account). After selecting one of them you have to give a start date and optionally an end date.

A List of the found mails is returned. You then kann exclude some of the mails from downloading its attachments. Therefore you can give a *space separated* list of the mail id's.

The mail attachments are downloaded and a copy of each file with the specified `suffix` is created.
Also a File named `received_content.json` is created. There one key is a mail address, and the value is an array of file paths to the files with suffix.

# Send Attachments back #

After you have done your work, e.g. annotated the copies of the received pdf files, you can send the mails back with the following command:

```python
mail_assistant.py 
  -s smtp.example.com 
  -u jonDoe 
  -c mail_content.txt  
  --subject My suggestions
  --sender your.email.com
  --receivers /home/user/Desktop/received_content.json
```

Where `mail_content.txt` is a text file containing the content of the all the mails you are about to send.